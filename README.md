rmongodb.quick
==========

Convenience functions and workaround for bugs in rmongodb.

<span style="color:red;">**2014-07-30**</span>: The functions were added to [rmongodb](https://github.com/mongosoup/rmongodb) by Dr. rer. nat. Markus Schmidberger.

### Installation

```R
install_github(repo = "rmongodb.quick", username = "stanstrup")
library(rmongodb.quick)
```

